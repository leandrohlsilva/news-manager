/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var news = require('./news.model');

exports.register = function(socket) {
  news = require('./news.model');
  news.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  news.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('news:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('news:remove', doc);
}