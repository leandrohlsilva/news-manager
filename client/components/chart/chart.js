/*global d3:false */
/*global nv:false */

'use strict';

angular.module('newsManagerApp')
  .directive('d3chart', function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        data:'=',
        chartId:'@',
        type:'@',
        height:'@'
      },
      template: '<div id="d{{chartId}}"><svg style="height:{{height}}px"></svg></div>',
      link: function(scope) {

        scope.$watch('data', function (data) {
          if (data) {
            nv.addGraph(function () {

              var chart;

              if (!scope.type || scope.type === 'multiBarHorizontal') {
                chart = nv.models.multiBarHorizontalChart();
                chart
                  .x(function (d) {
                    return d.label;
                  })
                  .y(function (d) {
                    return d.value;
                  })
                  .margin({top: 30, right: 20, bottom: 50, left: 175})
                  .showValues(true)           //Show bar value next to each bar.
                  .tooltips(true)
                  .stacked(false)
                  .transitionDuration(350)
                  .showControls(true);        //Allow user to switch between "Grouped" and "Stacked" mode.

                chart.yAxis
                  .tickFormat(d3.format(',.2f'));
              } else if (scope.type === 'line') {
                chart = nv.models.lineChart()
                  .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
                  .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
                  .transitionDuration(350)  //how fast do you want the lines to transition?
                  .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
                  .showYAxis(true)        //Show the y-axis
                  .showXAxis(true)        //Show the x-axis
                ;

                chart.xAxis     //Chart x-axis settings
                  .axisLabel('Time (ms)')
                  .tickFormat(d3.format(',r'));

                chart.yAxis     //Chart y-axis settings
                  .axisLabel('Voltage (v)')
                  .tickFormat(d3.format('.02f'));

              } else if (scope.type === 'multiBar') {
                chart = nv.models.multiBarChart();

                chart.xAxis
                  .tickFormat(d3.format(',f'));

                chart.yAxis
                  .tickFormat(d3.format(',.1f'));
              } else if (scope.type === 'pie') {
                chart = nv.models.pieChart()
                  .x(function(d) { return d.label })
                  .y(function(d) { return d.value })
                  .showLabels(true);
              }


              d3.select('#d' + scope.chartId + ' svg')
                .datum(data)
                .call(chart);

              nv.utils.windowResize(chart.update);

              return chart;
            });
          }
        });
      }
    };
  });