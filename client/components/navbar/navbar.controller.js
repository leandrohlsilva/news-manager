'use strict';

angular.module('newsManagerApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    }];

    $scope.menuAdmin = [{
      'title': 'Fontes',
      'link': '/fontes'
    }, {
      'title': 'Notícias',
      'link': '/noticias'
    }, {
      'title': 'Pesquisas',
      'link': '/pesquisas'
    }, {
      'title': 'Relatórios',
      'link': '/relatorios'
    }];

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });