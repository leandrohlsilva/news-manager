'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('newsManagerApp'));
  beforeEach(module('socketMock'));

  var MainCtrl,
      scope,
      $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, $controller, $rootScope) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/news')
      .respond([]);

    scope = $rootScope.$new();
    MainCtrl = $controller('TestimonialCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of enterprises to the scope', function () {
  //   $httpBackend.flush();
  //   expect(scope.awesomeThings.length).toBe(0);
  // });
});
