'use strict';

angular.module('newsManagerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('news', {
        url: '/noticias',
        templateUrl: 'app/news/news.html',
        controller: 'NewsCtrl'
      });
  });