'use strict';

angular.module('newsManagerApp')
  .controller('NewsCtrl', function ($scope, $http, socket) {
    $scope.sources = [];
    $scope.news = [];
    $scope.newNews = {};


    $http.get('/api/sources').success(function(sources) {
      $scope.sources = sources;
    });

    $http.get('/api/news').success(function(news) {
      $scope.news = news;
    });

    $scope.updateNews = function(news) {
      //$http.put('/api/news/' + news._id, news);

      //workaround. Remove it, then save it.
      $http.delete('/api/news/' + news._id).then(function() {
        $http.post('/api/news', news);
      });
    };

    $scope.deleteNews = function(news) {
      $http.delete('/api/news/' + news._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('news');
    });

    $scope.updateNews = function(source) {
      $http.get('/api/news', {params: {s: source.name}}).success(function(news) {
          $scope.news = news;
          socket.syncUpdates('news', $scope.news);
        });
    };

    $scope.$watch('selectedSource', function(source) {
      if (source) {
        $scope.updateNews(source);
      }
    });

  });
