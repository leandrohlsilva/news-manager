'use strict';

angular.module('newsManagerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('source', {
        url: '/fontes',
        templateUrl: 'app/source/source.html',
        controller: 'SourceCtrl'
      });
  });