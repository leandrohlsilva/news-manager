'use strict';

angular.module('newsManagerApp')
  .controller('SourceCtrl', function ($scope, $http, socket) {
    $scope.awesomeThings = [];

    $http.get('/api/sources').success(function(awesomeThings) {
      console.log(awesomeThings);
      $scope.awesomeThings = awesomeThings;
      socket.syncUpdates('source', $scope.awesomeThings);
    });

    $scope.addThing = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/sources', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/sources/' + thing._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('source');
    });
  });
