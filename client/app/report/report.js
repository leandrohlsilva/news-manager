'use strict';

angular.module('newsManagerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('report', {
        url: '/relatorios',
        templateUrl: 'app/report/report.html',
        controller: 'ReportCtrl'
      });
  });