'use strict';

angular.module('newsManagerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('research', {
        url: '/pesquisas',
        templateUrl: 'app/research/research.html',
        controller: 'ResearchCtrl'
      });
  });