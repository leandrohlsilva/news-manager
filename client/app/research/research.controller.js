'use strict';

angular.module('newsManagerApp')
  .controller('ResearchCtrl', function ($scope, $http, socket) {
    $scope.researches = [];
    $scope.newResearch = {};

    $scope.updateResearch = function(research) {
      //$http.put('/api/researches/' + research._id, research);
      //workaround. Remove it, then save it.
      $http.delete('/api/researches/' + research._id).then(function() {
        $http.post('/api/researches', research);
      });
    };

    $scope.addResearch = function() {
      $http.post('/api/researches', $scope.newResearch);
      $scope.newResearch = {};
    };

    $scope.deleteResearch = function(research) {
      $http.delete('/api/researches/' + research._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('research');
      if ($scope.listening) {
        $scope.toggleListening();
      }

    });

  });
