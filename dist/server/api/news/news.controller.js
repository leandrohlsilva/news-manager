/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /news              ->  index
 * POST    /news              ->  create
 * GET     /news/:id          ->  show
 * PUT     /news/:id          ->  update
 * DELETE  /news/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var AWS = require('aws-sdk');
var db = new AWS.DynamoDB();
var DynamoDBModel = require('dynamodb-model');

var newsSchema = new DynamoDBModel.Schema({
  link: {
    type: String,
    key: 'hash'
  },
  source: String,
  title: String,
  description: String,
  date: Number,
  pred_prob: Number,
  pred_label: String,
  real_label: String
});

var News = require('./news.model');

// Get list of news
exports.index = function(req, res) {
  News = new DynamoDBModel.Model('news', newsSchema);
  var source = req.param('s'); //Source may or may not be selected
  if (source === undefined) {
    News.scan({}, {Limit: 100}, function (err, data) {
      if (err) { return handleError(res, err); } 
      return res.json(200, data);
    });
  } else {
    var params = {
      TableName: 'news', /* required */
      IndexName: 'source-date-index',
      KeyConditions: { /* required */
        'source': {
          ComparisonOperator: 'EQ', /* required */
          AttributeValueList: [
            {
              S: source
            }
          ]
        }
      },
      //QueryFilter: {
      //  'real_label': {
      //    'ComparisonOperator': 'NULL'
      //  }
      //},
      Limit: 100,
      ReturnConsumedCapacity: 'TOTAL',
      ScanIndexForward: false,
      Select: 'ALL_ATTRIBUTES'
    };
    db.query(params, function(err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
        res.send(err);
      }
      else {
        var items = data.Items.map(newsSchema.mapFromDb.bind(newsSchema));
        res.json(items);
      }
    });
  }
};

// Get a single news
exports.show = function(req, res) {
  req.send([]);
};

// Creates a new news in the DB.
exports.create = function(req, res) {
  /*News.create(req.body, function(err, thing) {
    if(err) { return handleError(res, err); }
    return res.json(201, thing);
  });*/
  return res.json(201, null);
};

// Updates an existing news in the DB.
exports.update = function(req, res) {
  /*if(req.body._id) { delete req.body._id; }
  News.findById(req.params.id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    var updated = _.merge(thing, req.body);
    News.where({_id: req.params.id}).update(updated);
    updated.save(function (err, savedDoc, numberAffected) {
      console.log(err, savedDoc, numberAffected);
      if (err) { return handleError(res, err); }
      return res.json(200, thing);
    });
  });*/
  return res.json(200, null);
};

// Deletes a news from the DB.
exports.destroy = function(req, res) {
  /*News.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });*/
  return res.send(204);
};

function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}