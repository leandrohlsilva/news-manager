'use strict';

var DynamoDBModel = require('dynamodb-model');

var newsSchema = new DynamoDBModel.Schema({
  link: {
    type: String,
    key: 'hash'
  },
  source: String,
  title: String,
  description: String,
  date: Number,
  pred_prob: Number,
  pred_label: String,
  real_label: String
});

module.exports = new DynamoDBModel.Model('news', newsSchema);