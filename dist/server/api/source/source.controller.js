/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sources              ->  index
 * POST    /sources              ->  create
 * GET     /sources/:id          ->  show
 * PUT     /sources/:id          ->  update
 * DELETE  /sources/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var DynamoDBModel = require('dynamodb-model');

var newsSourceSchema = new DynamoDBModel.Schema({
  link: {
    type: String,
    key: 'hash'
  },
  name: String,
  labels: [String]
});
var Source = require('./source.model');

// Get list of enterprise
exports.index = function(req, res) {
  Source = new DynamoDBModel.Model('news_source', newsSourceSchema);
  Source.scan({}, {Limit: 100}, function (err, sources) {
    if(err) { return handleError(res, err); }
    return res.json(200, sources);
  });
};

// Get a single enterprise
exports.show = function(req, res) {
  /*Source.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    return res.json(thing);
  });*/
  return res.json(null);
};

// Creates a new source in the DB.
//TODO: include onSave function
exports.create = function(req, res) {
  /*Source.create(req.body, function(err, thing) {
    if(err) { return handleError(res, err); }
    return res.json(201, thing);
  });*/
  return res.json(201, null);
};

// Updates an existing enterprise in the DB.
exports.update = function(req, res) {
  /*if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    var updated = _.merge(thing, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, thing);
    });
  });*/
  return res.json(200, null);
};

// Deletes a enterprise from the DB.
exports.destroy = function(req, res) {
  /*Source.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });*/
  return res.send(204);
};

function handleError(res, err) {
  return res.send(500, err);
}