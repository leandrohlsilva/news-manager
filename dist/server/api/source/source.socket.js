/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var source = require('./source.model');

exports.register = function(socket) {
  source = require('./source.model');
  source.onSave = function (doc) {
    onSave(socket, doc);
  };
  source.onRemove = function (doc) {
    onRemove(socket, doc);
  };
}

function onSave(socket, doc, cb) {
  socket.emit('source:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('source:remove', doc);
}