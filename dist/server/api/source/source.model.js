'use strict';

var DynamoDBModel = require('dynamodb-model');

var newsSourceSchema = new DynamoDBModel.Schema({
  link: {
    type: String,
    key: 'hash'
  },
  name: String,
  labels: [String]
});

module.exports = new DynamoDBModel.Model('news_source', newsSourceSchema);