'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ResearchSchema = new Schema({
  text: {type: String, required: true, trim: true},
  user: String,
  date: { type: Date, default: Date.now },
  gender: String,
  sources: [String],
  classifications: [{}]
});

module.exports = mongoose.model('Research', ResearchSchema);