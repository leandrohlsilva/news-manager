/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /researches              ->  index
 * POST    /researches              ->  create
 * GET     /researches/:id          ->  show
 * PUT     /researches/:id          ->  update
 * DELETE  /researches/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Testimonial = require('./research.model');

// Get list of research
exports.index = function(req, res) {
  var filters = {};
  if (req.query.t) {
    filters = {enterprise: req.query.t};
  }

  Testimonial.find(filters, function (err, things) {
    if(err) { return handleError(res, err); }
    return res.json(200, things);
  });
};

// Get a single research
exports.show = function(req, res) {
  Testimonial.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    return res.json(thing);
  });
};

// Creates a new research in the DB.
exports.create = function(req, res) {
  Testimonial.create(req.body, function(err, thing) {
    if(err) { return handleError(res, err); }
    return res.json(201, thing);
  });
};

// Updates an existing research in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Testimonial.findById(req.params.id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    var updated = _.merge(thing, req.body);
    Testimonial.where({_id: req.params.id}).update(updated);
    updated.save(function (err, savedDoc, numberAffected) {
      console.log(err, savedDoc, numberAffected);
      if (err) { return handleError(res, err); }
      return res.json(200, thing);
    });
  });
};

// Deletes a research from the DB.
exports.destroy = function(req, res) {
  Testimonial.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}